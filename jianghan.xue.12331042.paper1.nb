(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     22335,        674]
NotebookOptionsPosition[     20306,        608]
NotebookOutlinePosition[     20931,        630]
CellTagsIndexPosition[     20888,        627]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Paper 1", "Title",
 CellChangeTimes->{{3.5988639772421846`*^9, 3.5988639845679173`*^9}, 
   3.6000714367019*^9}],

Cell["Partial Differential Equations", "Subtitle",
 CellChangeTimes->{{3.5988639772421846`*^9, 3.5988639845679173`*^9}, {
  3.6000714367019*^9, 3.6000714546209*^9}}],

Cell["\<\
Student name: Jianghan Xue
Student number: 12331042
Course: M.Sc. Computer Science (Interactive Entertainment Technology)
Year: 1
Module: CS7030 Numerical Methods and Advanced Mathematical Modelling - II
Instructor: Dr. Miche\[AAcute]l Mac An Airchinn\[IAcute]gh
Date: April/2014\
\>", "Text",
 CellChangeTimes->{{3.59886398999446*^9, 3.598864041747635*^9}, {
  3.60006936154852*^9, 3.6000693643718023`*^9}, {3.60006940084945*^9, 
  3.600069419434308*^9}, {3.606421907052327*^9, 3.6064219077743683`*^9}}],

Cell[CellGroupData[{

Cell["Exam 2013 Q1", "Section",
 CellChangeTimes->{{3.5988640456770277`*^9, 3.598864060651525*^9}}],

Cell[CellGroupData[{

Cell["I", "Subsection",
 CellChangeTimes->{{3.5988640556780276`*^9, 3.5988640654450045`*^9}}],

Cell["Consider the wave equation", "Text",
 CellChangeTimes->{{3.598864070087468*^9, 3.598864093285788*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    SuperscriptBox["\[Del]", "2"], "\[CurlyPhi]"}], "=", 
   RowBox[{
    FractionBox["1", 
     SuperscriptBox["c", "2"]], 
    FractionBox[
     RowBox[{
      SuperscriptBox["\[PartialD]", "2"], "\[CurlyPhi]"}], 
     RowBox[{"\[PartialD]", 
      SuperscriptBox["t", "2"]}]]}]}], TraditionalForm]], "Text",
 Evaluatable->False,
 CellChangeTimes->{{3.598864142919821*^9, 3.598864159462821*^9}, {
   3.598864258393821*^9, 3.5988642590328207`*^9}, {3.598864539358821*^9, 
   3.5988646026058207`*^9}, {3.598864729953458*^9, 3.5988647705545177`*^9}, 
   3.5988648145789194`*^9, {3.6063443945786376`*^9, 3.6063444097795067`*^9}, {
   3.6063445207858562`*^9, 3.60634452207193*^9}},
 TextAlignment->Center],

Cell[TextData[{
 "where ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["\[Del]", "2"], TraditionalForm]]],
 " is the Laplacian operator and \[CurlyPhi] abbreviates ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CurlyPhi]", "(", 
    RowBox[{
     OverscriptBox["x", "\[RightVector]"], ",", "t"}], ")"}], 
   TraditionalForm]]],
 ". WIth the aid of a suitable Ansatz separate the variables to obtain two \
independent ordinary differential equations and hence sketch out the details \
to the solution of the wave equation."
}], "Text",
 CellChangeTimes->{{3.5988647778222446`*^9, 3.598864865771038*^9}, {
  3.598864900181479*^9, 3.5988649155580163`*^9}, {3.5988649526537256`*^9, 
  3.598865011569617*^9}}],

Cell[CellGroupData[{

Cell["Solution:", "Subsubsection",
 CellChangeTimes->{{3.598865013742834*^9, 3.5988650250039597`*^9}}],

Cell["\<\
Based on [Gershenfeld] Chapter 3.3 Separation of Variables\
\>", "Text",
 CellChangeTimes->{{3.6063439943557463`*^9, 3.6063440010701303`*^9}, {
  3.606344071207142*^9, 3.6063441012298594`*^9}, {3.60634419338713*^9, 
  3.606344228632146*^9}, {3.606344277212925*^9, 3.6063442776369486`*^9}, {
  3.6063453651631517`*^9, 3.606345421719387*^9}}],

Cell[TextData[{
 "To start, the time dependence can be separated by assuming a solution of \
the form ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"\[CurlyPhi]", "(", 
     RowBox[{
      OverscriptBox["x", "\[RightVector]"], ",", "t"}], ")"}], "=", 
    RowBox[{
     RowBox[{"\[Psi]", "(", 
      OverscriptBox["x", "\[RightVector]"], ")"}], 
     RowBox[{"T", "(", "t", ")"}]}]}], TraditionalForm]],
  FormatType->"TraditionalForm"],
 ". There is no time dependence for Laplace\[CloseCurlyQuote]s equation; \
trying this in the wave equation gives"
}], "Text",
 CellChangeTimes->{{3.6063439943557463`*^9, 3.6063440010701303`*^9}, {
  3.606344071207142*^9, 3.6063441012298594`*^9}, {3.60634419338713*^9, 
  3.606344228632146*^9}, {3.606344277212925*^9, 3.6063442776369486`*^9}, {
  3.606345442948601*^9, 3.6063454443956833`*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    RowBox[{"T", "(", "t", ")"}], 
    RowBox[{
     SuperscriptBox["\[Del]", "2"], 
     RowBox[{"\[Psi]", "(", 
      OverscriptBox["x", "\[RightVector]"], ")"}]}]}], "=", 
   RowBox[{
    FractionBox["1", 
     SuperscriptBox["c", "2"]], 
    RowBox[{"\[Psi]", "(", 
     OverscriptBox["x", "\[RightVector]"], ")"}], 
    FractionBox[
     RowBox[{
      SuperscriptBox["\[PartialD]", "2"], 
      RowBox[{"T", "(", "t", ")"}]}], 
     RowBox[{"\[PartialD]", 
      SuperscriptBox["t", "2"]}]]}]}], TraditionalForm]], "Text",
 CellChangeTimes->{{3.6063442840763173`*^9, 3.6063443788027353`*^9}, {
  3.6063445480154133`*^9, 3.6063445742869167`*^9}, {3.6063446437528896`*^9, 
  3.606344730902874*^9}, {3.606344779794671*^9, 3.606344807825274*^9}},
 TextAlignment->Center],

Cell[TextData[{
 "Dividing both sides by \[Psi]T results in no t dependence on the left hand \
side and no ",
 Cell[BoxData[
  FormBox[
   OverscriptBox["x", "\[RightVector]"], TraditionalForm]],
  CellChangeTimes->{{3.6063442840763173`*^9, 3.6063443788027353`*^9}, {
   3.6063445480154133`*^9, 3.6063445742869167`*^9}, {3.6063446437528896`*^9, 
   3.606344730902874*^9}, {3.606344779794671*^9, 3.606344807825274*^9}},
  TextAlignment->Center],
 " dependence on the right hand side, so both sides must be equal to some \
constant because the space and time variables can be varied arbitrarily. By \
convention, taking this constant to be ",
 Cell[BoxData[
  FormBox[
   RowBox[{"-", 
    SuperscriptBox["k", "2"]}], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " gives"
}], "Text",
 CellChangeTimes->{{3.6063448531388655`*^9, 3.6063449410238924`*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    FractionBox["1", 
     RowBox[{"\[Psi]", 
      RowBox[{"(", 
       OverscriptBox["x", "\[RightVector]"], ")"}]}]], 
    RowBox[{
     SuperscriptBox["\[Del]", "2"], "\[Psi]"}], 
    RowBox[{"(", 
     OverscriptBox["x", "\[RightVector]"], ")"}]}], "=", 
   RowBox[{
    RowBox[{
     FractionBox["1", 
      SuperscriptBox["c", "2"]], 
     FractionBox["1", 
      RowBox[{"T", "(", "t", ")"}]], 
     FractionBox[
      RowBox[{
       SuperscriptBox["\[DifferentialD]", "2"], "T"}], 
      RowBox[{"\[DifferentialD]", 
       SuperscriptBox["t", "2"]}]]}], "=", 
    RowBox[{"-", 
     SuperscriptBox["k", "2"]}]}]}], TraditionalForm]], "Text",
 CellChangeTimes->{{3.6063442840763173`*^9, 3.6063443788027353`*^9}, {
  3.6063445480154133`*^9, 3.6063445742869167`*^9}, {3.6063446437528896`*^9, 
  3.606344730902874*^9}, {3.606344779794671*^9, 3.606344783256869*^9}, {
  3.606344937011663*^9, 3.6063450439867816`*^9}},
 TextAlignment->Center],

Cell["\<\
The t equation can immediately be integrated to find\
\>", "Text",
 CellChangeTimes->{{3.606345077970725*^9, 3.6063450830070133`*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{"T", "(", "t", ")"}], "=", 
   RowBox[{
    RowBox[{"A", " ", 
     RowBox[{"sin", "(", 
      RowBox[{"k", " ", "c", " ", "t"}], ")"}]}], "+", 
    RowBox[{"B", " ", 
     RowBox[{"sin", "(", 
      RowBox[{"k", " ", "c", " ", "t"}], ")"}]}]}]}], 
  TraditionalForm]], "Text",
 CellChangeTimes->{{3.606345105005272*^9, 3.606345143384467*^9}},
 TextAlignment->Center],

Cell[TextData[{
 "and the ",
 Cell[BoxData[
  FormBox[
   OverscriptBox["x", "\[RightVector]"], TraditionalForm]],
  CellChangeTimes->{{3.6063442840763173`*^9, 3.6063443788027353`*^9}, {
   3.6063445480154133`*^9, 3.6063445742869167`*^9}, {3.6063446437528896`*^9, 
   3.606344730902874*^9}, {3.606344779794671*^9, 3.606344783256869*^9}, {
   3.606344937011663*^9, 3.6063450439867816`*^9}},
  TextAlignment->Center],
 " equation is Helmholtz\[CloseCurlyQuote]s equation"
}], "Text",
 CellChangeTimes->{{3.6063452699057035`*^9, 3.6063452823214135`*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    RowBox[{
     SuperscriptBox["\[Del]", "2"], 
     RowBox[{"\[Psi]", "(", 
      OverscriptBox["x", "\[RightVector]"], ")"}]}], "+", 
    RowBox[{
     SuperscriptBox["k", "2"], 
     RowBox[{"\[Psi]", "(", 
      OverscriptBox["x", "\[RightVector]"], ")"}]}]}], "=", "0"}], 
  TraditionalForm]], "Text",
 CellChangeTimes->{{3.606345105005272*^9, 3.606345143384467*^9}, {
  3.606345296383218*^9, 3.606345309388962*^9}},
 TextAlignment->Center],

Cell["\<\
Solving Helmholtz\[CloseCurlyQuote]s equation will depend on the coordinate \
system used for the problem. There are three common ones used in 3D, based on \
the symmetry of the problem: rectangular, cylindrical, and spherical. Writing \
the derivative operators in each of these systems is a straightforward \
exercise in applying the chain rule to the coordinate definitions.\
\>", "Text",
 CellChangeTimes->{{3.6063452699057035`*^9, 3.6063452823214135`*^9}, 
   3.6063453450300007`*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["II", "Subsection",
 CellChangeTimes->{{3.5994645560444903`*^9, 3.5994645654450665`*^9}}],

Cell["Consider the 1D diffusion equation:", "Text",
 CellChangeTimes->{{3.599464571589364*^9, 3.599464588914483*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   FractionBox[
    RowBox[{
     SuperscriptBox["\[PartialD]", "2"], "\[CurlyPhi]"}], 
    RowBox[{"\[PartialD]", 
     SuperscriptBox["x", "2"]}]], "=", 
   RowBox[{
    FractionBox["1", "D"], 
    FractionBox[
     RowBox[{"\[PartialD]", "\[CurlyPhi]"}], 
     RowBox[{"\[PartialD]", "t"}]]}]}], TraditionalForm]], "Text",
 CellChangeTimes->{{3.5994646175335026`*^9, 3.599464672513962*^9}, {
  3.599464723986331*^9, 3.599464732636925*^9}, {3.5994648191848817`*^9, 
  3.5994648640543385`*^9}},
 TextAlignment->Center],

Cell[TextData[{
 "where \[CurlyPhi] abbreviates ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CurlyPhi]", 
    RowBox[{"(", 
     RowBox[{"x", ",", " ", "t"}], ")"}]}], TraditionalForm]]],
 " and D, presumably, denotes some sort of diffusion constant. In his \
application of the Fourier transform, Gershenfeld (p.268), presents the \
solution to the diffusion equation firstly in the forward transform"
}], "Text",
 CellChangeTimes->{{3.5994648692749043`*^9, 3.5994649257048283`*^9}, {
  3.599464999739032*^9, 3.59946503791689*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   FractionBox[
    RowBox[{"\[PartialD]", 
     RowBox[{"\[CapitalPhi]", "(", 
      RowBox[{"k", ",", " ", "t"}], ")"}]}], 
    RowBox[{"\[PartialD]", "t"}]], "=", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"-", "D"}], " ", 
      SuperscriptBox["k", "2"], " ", 
      RowBox[{"\[CapitalPhi]", "(", 
       RowBox[{"k", ",", " ", "t"}], ")"}]}], "\[Implies]", 
     RowBox[{"\[CapitalPhi]", "(", 
      RowBox[{"k", ",", " ", "t"}], ")"}]}], "=", 
    RowBox[{
     RowBox[{"A", "(", "k", ")"}], 
     SuperscriptBox["e", 
      RowBox[{
       RowBox[{"-", "D"}], " ", 
       SuperscriptBox["k", "2"], " ", "t"}]]}]}]}], TraditionalForm]], "Text",
 CellChangeTimes->{{3.599465041145213*^9, 3.5994650885379515`*^9}, {
  3.599465125354633*^9, 3.5994651965287495`*^9}, {3.5994652407041664`*^9, 
  3.599465282483344*^9}},
 TextAlignment->Center],

Cell["\<\
and then the solution via the inverse Fourier transform. What is this \
solution?\
\>", "Text",
 CellChangeTimes->{{3.5994652869147873`*^9, 3.599465308264922*^9}}],

Cell[CellGroupData[{

Cell["Solution:", "Subsubsection",
 CellChangeTimes->{{3.598865013742834*^9, 3.5988650250039597`*^9}}],

Cell["\<\
Based on [Gershenfeld] A4.3 Partial Differential Equations\
\>", "Text",
 CellChangeTimes->{{3.6063439943557463`*^9, 3.6063440010701303`*^9}, {
  3.606344071207142*^9, 3.6063441012298594`*^9}, {3.60634419338713*^9, 
  3.606344228632146*^9}, {3.606344277212925*^9, 3.6063442776369486`*^9}, {
  3.6063453651631517`*^9, 3.606345421719387*^9}, {3.606423605614479*^9, 
  3.6064236167311153`*^9}}],

Cell[BoxData[
 FormBox[
  RowBox[{
   FractionBox[
    RowBox[{
     SuperscriptBox["\[PartialD]", "2"], 
     RowBox[{"\[CurlyPhi]", "(", 
      RowBox[{"x", ",", "t"}], ")"}]}], 
    RowBox[{"\[PartialD]", 
     SuperscriptBox["x", "2"]}]], "=", 
   RowBox[{
    FractionBox["1", "D"], 
    FractionBox[
     RowBox[{"\[PartialD]", 
      RowBox[{"\[CurlyPhi]", "(", 
       RowBox[{"x", ",", "t"}], ")"}]}], 
     RowBox[{"\[PartialD]", "t"}]]}]}], TraditionalForm]], "Text",
 CellChangeTimes->{{3.599465041145213*^9, 3.5994650885379515`*^9}, {
  3.599465125354633*^9, 3.5994651965287495`*^9}, {3.5994652407041664`*^9, 
  3.599465282483344*^9}, {3.606345613731369*^9, 3.606345663422211*^9}},
 TextAlignment->Center],

Cell[BoxData[{
 FormBox[
  RowBox[{
   RowBox[{"\[CurlyPhi]", "(", 
    RowBox[{"x", ",", "t"}], ")"}], "=", 
   RowBox[{
    SubsuperscriptBox["\[Integral]", 
     RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
    RowBox[{
     RowBox[{"\[CapitalPhi]", "(", 
      RowBox[{"k", ",", " ", "t"}], ")"}], 
     SuperscriptBox["e", 
      RowBox[{"i", " ", "k", " ", "x"}]], 
     RowBox[{"\[DifferentialD]", "k"}]}]}]}], 
  TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{
   RowBox[{"\[CapitalPhi]", "(", 
    RowBox[{"k", ",", " ", "t"}], ")"}], "=", 
   RowBox[{
    FractionBox["1", 
     RowBox[{"2", "\[Pi]"}]], 
    RowBox[{
     SubsuperscriptBox["\[Integral]", 
      RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
     RowBox[{
      RowBox[{"\[CurlyPhi]", "(", 
       RowBox[{"x", ",", "t"}], ")"}], 
      SuperscriptBox["e", 
       RowBox[{
        RowBox[{"-", "i"}], " ", "k", " ", "x"}]], 
      RowBox[{"\[DifferentialD]", "x"}]}]}]}]}], TraditionalForm]}], "Text",
 CellChangeTimes->{{3.599465041145213*^9, 3.5994650885379515`*^9}, {
  3.599465125354633*^9, 3.5994651965287495`*^9}, {3.5994652407041664`*^9, 
  3.599465282483344*^9}, {3.606345613731369*^9, 3.6063458130527697`*^9}},
 TextAlignment->Center],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{
    SubsuperscriptBox["\[Integral]", 
     RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
    RowBox[{
     SuperscriptBox["e", 
      RowBox[{"i", " ", "k", " ", "x"}]], "[", 
     RowBox[{
      RowBox[{
       FractionBox["1", "D"], 
       FractionBox[
        RowBox[{"\[PartialD]", 
         RowBox[{"\[CapitalPhi]", "(", 
          RowBox[{"k", ",", "t"}], ")"}]}], 
        RowBox[{"\[PartialD]", "t"}]]}], "+", 
      RowBox[{
       SuperscriptBox["k", "2"], 
       RowBox[{"\[CapitalPhi]", "(", 
        RowBox[{"k", ",", "t"}], ")"}]}]}], "]"}]}], "=", "0"}], 
  TraditionalForm]], "Text",
 CellChangeTimes->{{3.599465041145213*^9, 3.5994650885379515`*^9}, {
  3.599465125354633*^9, 3.5994651965287495`*^9}, {3.5994652407041664`*^9, 
  3.599465282483344*^9}, {3.606345613731369*^9, 3.6063458730912037`*^9}, {
  3.606345932417597*^9, 3.6063459794112844`*^9}},
 TextAlignment->Center],

Cell[BoxData[
 FormBox[
  RowBox[{
   FractionBox[
    RowBox[{"\[PartialD]", 
     RowBox[{"\[CapitalPhi]", "(", 
      RowBox[{"k", ",", "t"}], ")"}]}], 
    RowBox[{"\[PartialD]", "t"}]], "=", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"-", "D"}], " ", 
      SuperscriptBox["k", "2"], 
      RowBox[{"\[CapitalPhi]", "(", 
       RowBox[{"k", ",", "t"}], ")"}]}], "\[DoubleRightArrow]", 
     RowBox[{"\[CapitalPhi]", "(", 
      RowBox[{"k", ",", "t"}], ")"}]}], "=", 
    RowBox[{
     RowBox[{"A", "(", "k", ")"}], 
     SuperscriptBox["e", 
      RowBox[{
       RowBox[{"-", "D"}], " ", 
       SuperscriptBox["k", "2"], "t"}]]}]}]}], TraditionalForm]], "Text",
 CellChangeTimes->{{3.599465041145213*^9, 3.5994650885379515`*^9}, {
  3.599465125354633*^9, 3.5994651965287495`*^9}, {3.5994652407041664`*^9, 
  3.599465282483344*^9}, {3.606345613731369*^9, 3.6063458730912037`*^9}, {
  3.606345932417597*^9, 3.6063460524954653`*^9}},
 TextAlignment->Center],

Cell[BoxData[
 FormBox[
  RowBox[{
   RowBox[{"\[CapitalPhi]", "(", 
    RowBox[{"k", ",", "0"}], ")"}], "=", 
   RowBox[{
    RowBox[{
     FractionBox["1", 
      RowBox[{"2", "\[Pi]"}]], 
     RowBox[{
      SubsuperscriptBox["\[Integral]", 
       RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
      RowBox[{
       RowBox[{"\[CurlyPhi]", "(", 
        RowBox[{"x", ",", "0"}], ")"}], 
       SuperscriptBox["e", 
        RowBox[{
         RowBox[{"-", "i"}], " ", "k", " ", "x"}]], 
       RowBox[{"\[DifferentialD]", "x"}]}]}]}], "=", 
    RowBox[{
     RowBox[{
      RowBox[{"A", "(", "k", ")"}], 
      SuperscriptBox["e", "0"]}], "=", 
     RowBox[{"A", "(", "k", ")"}]}]}]}], TraditionalForm]], "Text",
 CellChangeTimes->{{3.599465041145213*^9, 3.5994650885379515`*^9}, {
  3.599465125354633*^9, 3.5994651965287495`*^9}, {3.5994652407041664`*^9, 
  3.599465282483344*^9}, {3.606345613731369*^9, 3.6063458730912037`*^9}, {
  3.606345932417597*^9, 3.60634612723474*^9}},
 TextAlignment->Center],

Cell[BoxData[{
 FormBox[
  RowBox[{
   RowBox[{"\[CurlyPhi]", "(", 
    RowBox[{"x", ",", "t"}], ")"}], "=", 
   RowBox[{
    SubsuperscriptBox["\[Integral]", 
     RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
    RowBox[{
     RowBox[{"[", 
      RowBox[{
       FractionBox["1", 
        RowBox[{"2", "\[Pi]"}]], 
       RowBox[{
        SubsuperscriptBox["\[Integral]", 
         RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
        RowBox[{
         RowBox[{"\[CurlyPhi]", "(", 
          RowBox[{
           RowBox[{"x", "'"}], ",", "0"}], ")"}], 
         SuperscriptBox["e", 
          RowBox[{
           RowBox[{"-", "i"}], " ", "k", " ", 
           RowBox[{"x", "'"}]}]], 
         RowBox[{"\[DifferentialD]", 
          RowBox[{"x", "'"}]}]}]}]}], "]"}], 
     SuperscriptBox["e", 
      RowBox[{
       RowBox[{"-", "D"}], " ", 
       SuperscriptBox["k", "2"], "t"}]], 
     SuperscriptBox["e", 
      RowBox[{
       RowBox[{"-", "i"}], " ", "k", " ", "x"}]], 
     RowBox[{"\[DifferentialD]", "k"}]}]}]}], 
  TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"=", 
   RowBox[{
    FractionBox["1", 
     RowBox[{"2", "\[Pi]"}]], 
    RowBox[{
     SubsuperscriptBox["\[Integral]", 
      RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
     RowBox[{
      RowBox[{"\[CurlyPhi]", "(", 
       RowBox[{
        RowBox[{"x", "'"}], ",", "0"}], ")"}], 
      RowBox[{
       SubsuperscriptBox["\[Integral]", 
        RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
       RowBox[{
        SuperscriptBox["e", 
         RowBox[{
          RowBox[{
           RowBox[{"-", "D"}], " ", 
           SuperscriptBox["k", "2"], "t"}], "-", 
          RowBox[{"i", " ", "k", " ", 
           RowBox[{"x", "'"}]}], "+", 
          RowBox[{"i", " ", "k", " ", "x"}]}]], 
        RowBox[{"\[DifferentialD]", "k"}], 
        RowBox[{"\[DifferentialD]", 
         RowBox[{"x", "'"}]}]}]}]}]}]}]}], 
  TraditionalForm], "\[IndentingNewLine]", 
 FormBox[
  RowBox[{"=", 
   RowBox[{
    FractionBox["1", 
     SqrtBox[
      RowBox[{"4", "\[Pi]D", " ", "t"}]]], 
    RowBox[{
     SubsuperscriptBox["\[Integral]", 
      RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
     RowBox[{
      SuperscriptBox["e", 
       RowBox[{"-", 
        FractionBox[
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"x", "-", 
            RowBox[{"x", "'"}]}], ")"}], "2"], 
         RowBox[{"4", "D", " ", "t"}]]}]], 
      RowBox[{"\[CurlyPhi]", "(", 
       RowBox[{
        RowBox[{"x", "'"}], ",", "0"}], ")"}], 
      RowBox[{"\[DifferentialD]", 
       RowBox[{"x", "'"}]}]}]}]}]}], TraditionalForm]}], "Text",
 CellChangeTimes->{{3.599465041145213*^9, 3.5994650885379515`*^9}, {
  3.599465125354633*^9, 3.5994651965287495`*^9}, {3.5994652407041664`*^9, 
  3.599465282483344*^9}, {3.606345613731369*^9, 3.6063458730912037`*^9}, {
  3.606345932417597*^9, 3.6063463981702366`*^9}},
 TextAlignment->Center],

Cell["\<\
The solution is the initial condition convolved with a spreading Gaussian.\
\>", "Text",
 CellChangeTimes->{{3.6063464107939587`*^9, 3.6063464295390306`*^9}}]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{944, 1057},
WindowMargins->{{Automatic, 20}, {Automatic, 325}},
Magnification->1.8000001907348633`,
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[StyleDefinitions -> "Default.nb"]], 
   Cell[
    StyleData[All]]}, Visible -> False, FrontEndVersion -> 
  "9.0 for Microsoft Windows (64-bit) (January 25, 2013)", StyleDefinitions -> 
  "PrivateStylesheetFormatting.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 118, 2, 160, "Title"],
Cell[700, 26, 165, 2, 88, "Subtitle"],
Cell[868, 30, 514, 11, 321, "Text"],
Cell[CellGroupData[{
Cell[1407, 45, 99, 1, 141, "Section"],
Cell[CellGroupData[{
Cell[1531, 50, 93, 1, 77, "Subsection"],
Cell[1627, 53, 108, 1, 53, "Text"],
Cell[1738, 56, 749, 19, 89, "Text",
 Evaluatable->False],
Cell[2490, 77, 700, 18, 161, "Text"],
Cell[CellGroupData[{
Cell[3215, 99, 102, 1, 60, "Subsubsection"],
Cell[3320, 102, 350, 6, 53, "Text"],
Cell[3673, 110, 839, 20, 159, "Text"],
Cell[4515, 132, 819, 23, 89, "Text"],
Cell[5337, 157, 860, 20, 189, "Text"],
Cell[6200, 179, 994, 29, 91, "Text"],
Cell[7197, 210, 144, 3, 53, "Text"],
Cell[7344, 215, 413, 13, 53, "Text"],
Cell[7760, 230, 551, 12, 53, "Text"],
Cell[8314, 244, 494, 15, 56, "Text"],
Cell[8811, 261, 499, 8, 221, "Text"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9359, 275, 94, 1, 77, "Subsection"],
Cell[9456, 278, 117, 1, 53, "Text"],
Cell[9576, 281, 554, 16, 89, "Text"],
Cell[10133, 299, 530, 12, 154, "Text"],
Cell[10666, 313, 891, 26, 80, "Text"],
Cell[11560, 341, 173, 4, 87, "Text"],
Cell[CellGroupData[{
Cell[11758, 349, 102, 1, 60, "Subsubsection"],
Cell[11863, 352, 401, 7, 53, "Text"],
Cell[12267, 361, 718, 20, 89, "Text"],
Cell[12988, 383, 1239, 35, 153, "Text"],
Cell[14230, 420, 942, 26, 83, "Text"],
Cell[15175, 448, 974, 27, 80, "Text"],
Cell[16152, 477, 1004, 28, 83, "Text"],
Cell[17159, 507, 2924, 90, 237, "Text"],
Cell[20086, 599, 168, 3, 87, "Text"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
